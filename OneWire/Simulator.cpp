#include "stdafx.h"
#include <inttypes.h>
#include "OneWire.h"
#include <Windows.h>

// Platform specific I/O definitions
#include <vector>
std::vector <bool> buffer;

static int temp = 27;
void gpio_write(bool status)
{
	if (status)
	{
		printf("W1 ");
	}
	else
	{
		printf("W0 ");
	}
	buffer.push_back(status);
}

bool gpio_read(void)
{
	bool retVal = false;
	if (buffer.size())
	{
		retVal = buffer.back();
		buffer.pop_back();
	}
	else
	{
		retVal = true;
	}
	if (retVal)
	{
		printf("R1 ");
	}
	else
	{
		printf("R0 ");
	}

	return retVal;
}

void gpio_set_mode(uint32_t mode)
{
	if (mode == GPIO_INPUT)
	{
		printf("\nINPUT\n");
	}
	else
	{
		printf("\nOUTPUT\n");
	}
}
void gpio_delay(uint32_t delay)
{
		__int64 time1 = 0, time2 = 0, freq = 0;

		QueryPerformanceCounter((LARGE_INTEGER *)&time1);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);

		do {
			QueryPerformanceCounter((LARGE_INTEGER *)&time2);
		} while ((time2 - time1) < delay);
}