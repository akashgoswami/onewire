// OneWire.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "OneWire.h"
#include "Windows.h"

#define byte uint8_t
#define delay(x) Sleep(x)


int _tmain(int argc, _TCHAR* argv[])
{
	OneWire ds(5);
	byte i;
	byte present = 0;
	byte type_s;
	byte data[12];
	byte addr[8];


	if (!ds.search(addr)) {
		printf("No more addresses.");
		ds.reset_search();
		delay(250);
		return 0;
	}

	printf("ROM =");
	for (i = 0; i < 8; i++) {
		printf(" ");
		printf("%x",addr[i]);
	}

	if (OneWire::crc8(addr, 7) != addr[7]) {
		printf("CRC is not valid!");
		return 0;
	}
	printf("\n");

	// the first ROM byte indicates which chip
	switch (addr[0]) {
	case 0x10:
		printf("  Chip = DS18S20");  // or old DS1820
		type_s = 1;
		break;
	case 0x28:
		printf("  Chip = DS18B20");
		type_s = 0;
		break;
	case 0x22:
		printf("  Chip = DS1822");
		type_s = 0;
		break;
	default:
		printf("Device is not a DS18x20 family device.");
		return 0;
	}

	ds.reset();
	ds.select(addr);
	ds.write(0x44, 1);        // start conversion, with parasite power on at the end

	delay(1000);     // maybe 750ms is enough, maybe not
	// we might do a ds.depower() here, but the reset will take care of it.

	present = ds.reset();
	ds.select(addr);
	ds.write(0xBE);         // Read Scratchpad

	printf("  Data = ");
	printf("%x",present);
	printf(" ");
	for (i = 0; i < 9; i++) {           // we need 9 bytes
		data[i] = ds.read();
		printf("%x",data[i]);
		printf(" ");
	}
	printf(" CRC=");
	printf("%x",OneWire::crc8(data, 8));
	printf("\n");

	// Convert the data to actual temperature
	// because the result is a 16 bit signed integer, it should
	// be stored to an "int16_t" type, which is always 16 bits
	// even when compiled on a 32 bit processor.
	int16_t raw = (data[1] << 8) | data[0];
	if (type_s) {
		raw = raw << 3; // 9 bit resolution default
		if (data[7] == 0x10) {
			// "count remain" gives full 12 bit resolution
			raw = (raw & 0xFFF0) + 12 - data[6];
		}
	}
	else {
		byte cfg = (data[4] & 0x60);
		// at lower res, the low bits are undefined, so let's zero them
		if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
		else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
		else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
		//// default is 12 bit resolution, 750 ms conversion time
	}
	return 0;
}

